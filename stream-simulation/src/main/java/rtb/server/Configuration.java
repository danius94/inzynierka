package rtb.server;

import lombok.Data;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.text.WordUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.function.Supplier;

/**
 * Created by daniel on 06.10.16.
 */
@Data
public class Configuration {

    private static final Map<String,Field> FIELD_MAP = new HashMap<>();
    static {
        Arrays.asList(Field.values()).forEach(field -> field.getOptionNames().forEach(x -> FIELD_MAP.put(x,field)));
    }

    private String host = "localhost";
    private Integer port = 7708;
    private List<String> inputFiles = new ArrayList<>();
    private String separator = "\t";
    private Integer timestamp = 1;
    private Long speed = 1L ;


    public void setOption(String name, String value){
        setOption(FIELD_MAP.get(name.replaceFirst("^-+","")),value);
    }

    public void addInputFile(String file){
        inputFiles.add(file);
    }

    public void setOption(Field field, String value){
        field.setValue(this,value);
    }

    public enum Field {
        PORT(Arrays.asList("p", "port")),
        HOST(Arrays.asList("h", "host")),
        SPEED(Arrays.asList("s", "speed")),
        TIMESTAMP(Arrays.asList("t", "timestamp")),
        ;

        private final String propertyName = WordUtils.uncapitalize(WordUtils.capitalizeFully(this.name().replaceAll("_"," ")).replaceAll(" ",""));
        private final List<String> optionNames;

        Field() {
            this(Collections.emptyList());
        }

        Field(List<String> optionNames) {
            this.optionNames = optionNames;
        }

        public List<String> getOptionNames() {
            return optionNames;
        }

        public Object getValue(Configuration configuration){
            try {
                return PropertyUtils.getProperty(configuration,propertyName);
            } catch(IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }

        public void setValue(Configuration configuration, String value){
            Object val = value;
            if(EnumSet.of(PORT,TIMESTAMP).contains(this)){
                val = Integer.parseInt(value);
            } else if(EnumSet.of(SPEED).contains(this)){
                val = Long.parseLong(value);
            }

            try {
                PropertyUtils.setProperty(configuration,propertyName,val);
            } catch(IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
