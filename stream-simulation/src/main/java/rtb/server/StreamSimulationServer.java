package rtb.server;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Callable;
import java.util.stream.Stream;

import static org.apache.commons.collections4.ListUtils.emptyIfNull;

/**
 * Created by daniel on 06.10.16.
 */
public class StreamSimulationServer {

    private static final long NANOS_IN_SECOND = 1_000_000L;
    public static final DateTimeFormatter DATE_FORMAT = new DateTimeFormatterBuilder().appendPattern("yyyyMMddHHmmss").appendValue(ChronoField.MILLI_OF_SECOND, 3).toFormatter();

    @Setter
    @Getter
    private Configuration configuration;
    private Socket socket;
    private int counter = 0;

    public void start() throws IOException {
            socket = new Socket(configuration.getHost(),configuration.getPort());
            write();
        IOUtils.closeQuietly(socket);
    }

    protected void write() throws IOException{
        try (BufferedWriter output = getOutput()) {
            TimestampHolder timestampHolder = new TimestampHolder();

            getInput().forEach(line-> {
                if(timestampHolder.getThisTimestamp() != null) {
                    timestampHolder.setPrevTimestamp(timestampHolder.getThisTimestamp());
                }
                timestampHolder.setThisTimestamp(extractTimestamp(line));
                if (timestampHolder.getPrevTimestamp() != null && timestampHolder.getThisTimestamp() != null) {
                    long sleepTime = ChronoUnit.NANOS.between(timestampHolder.getPrevTimestamp(), timestampHolder.getThisTimestamp());
                    sleepTime = sleepTime / configuration.getSpeed();
                    if(sleepTime<0){
                        sleepTime=0;
                    }
                    try {
                        Thread.sleep(sleepTime / NANOS_IN_SECOND, (int) (sleepTime % NANOS_IN_SECOND));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                eatException(()->{output.write(line);output.write("\n");output.flush();return null;});
                counter++;
                if(counter==10_000){
                    System.err.print(".");
                    counter=0;
                }
            });
        }
    }

    protected BufferedWriter getOutput() throws IOException {
        return new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    protected Stream<String> getInput() throws IOException {
        return emptyIfNull(configuration.getInputFiles()).stream()
                .map(Paths::get)
                .map(file -> eatException(() -> Files.lines(file, StandardCharsets.UTF_8)))
                .reduce(Stream::concat)
                .orElse(Stream.<String>empty());
    }

    protected LocalDateTime extractTimestamp(String data) {
        if(StringUtils.isBlank(data) || data.startsWith("@")){
            return null;
        }
        String timestamp = data.split(configuration.getSeparator())[1];
        return LocalDateTime.parse(timestamp,DATE_FORMAT);
    }

    private static <T> T eatException(Callable<T> callable){
        try {
            return callable.call();
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Data
    class TimestampHolder{
        private LocalDateTime prevTimestamp = null;
        private LocalDateTime thisTimestamp = null;
    }
}
