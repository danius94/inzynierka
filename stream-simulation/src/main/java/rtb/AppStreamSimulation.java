package rtb;

import rtb.server.Configuration;
import rtb.server.StreamSimulationServer;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by daniel on 06.10.16.
 */
public class AppStreamSimulation {

    public static void main(String[] args) throws IOException {
        StreamSimulationServer streamSimulationServer = new StreamSimulationServer();
        streamSimulationServer.setConfiguration(configure(args));
        streamSimulationServer.start();
    }

    private static Configuration configure(String[] args){
        Iterator<String> it = Arrays.asList(args).iterator();
        Configuration configuration = new Configuration();
        while(it.hasNext()){
            String arg1 = it.next();
            if(arg1.startsWith("-")){
                configuration.setOption(arg1,it.next());
            }else {
                configuration.addInputFile(arg1);
            }
        }
        return configuration;
    }
}
