package moa.streams;

import com.github.javacliparser.IntOption;
import com.github.javacliparser.StringOption;
import com.yahoo.labs.samoa.instances.Instances;
import moa.core.InputStreamProgressMonitor;

import java.io.*;
import java.net.Socket;

/**
 * Created by Daniel Tyka on 2016-10-10.
 */
public class ArffSocketStream extends ArffFileStream {

    @Override
    public String getPurposeString() {
        return "A stream read from a socket (in ARFF format) .";
    }

    public StringOption hostName = new StringOption(
            "hostName",
            'h',
            "Host from which to load data",
            "localhost"
    );

    public IntOption port = new IntOption(
            "port",
            'p',
            "Port from which to load data",
            12345,
            1,
            65535
    );

    protected Socket socket;

    public ArffSocketStream() {
    }

    public ArffSocketStream(String arffFileName, String hostName, int port) {
        this.arffFileOption.setValue(arffFileName);
        this.hostName.setValue(hostName);
        this.port.setValue(port);
        restart();
    }

    @Override
    public void restart() {
        try {
            if (this.fileReader != null) {
                this.fileReader.close();
            }
            if(this.socket != null){
                this.socket.close();
            }
            socket = new Socket(hostName.getValue(),port.getValue());
            this.fileProgressMonitor = new InputStreamProgressMonitor(
                    socket.getInputStream());
            this.fileReader = new BufferedReader(new InputStreamReader(
                    this.fileProgressMonitor));
            int classIndex = this.classIndexOption.getValue();
            this.instances = new Instances(this.fileReader, 1, classIndex);
            if (classIndex < 0) {
                this.instances.setClassIndex(this.instances.numAttributes() - 1);
            } else if (this.classIndexOption.getValue() > 0) {
                this.instances.setClassIndex(this.classIndexOption.getValue() - 1);
            }
            this.numInstancesRead = 0;
            this.lastInstanceRead = null;
            this.hitEndOfFile = !readNextInstanceFromFile();
        } catch (IOException ioe) {
            throw new RuntimeException("ArffFileStream restart failed.", ioe);
        }
    }



}
