package rtb;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import rtb.data.*;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 * Created by daniel on 02.10.16.
 */
public class AppFilterData {

    private FilteredBidLabels stats;
    private List<BidLogFields> fields;
    private Path outputFile;
    private PrintStream tempStream;
    private File tempFile;
    private int progress=0;

    public static void main(String[] args) throws IOException {
        String outputFile = null;
        String outputFields = null;
        int maxLines = Integer.MAX_VALUE;
        Integer randomLines = null;

        List<String> files = new ArrayList<>();
        for(int i = 0;i<args.length;i++){
            switch(args[i]){
                case "-o": outputFile = args[++i]; break;
                case "-f": outputFields = args[++i]; break;
                case "-n": maxLines = Integer.parseInt(args[++i]); break;
                case "-r": randomLines = Integer.parseInt(args[++i]); break;
                default: files.add(args[i]);
            }
        }
        files.forEach(System.err::println);

        Stream<BidLogEntry> inputData = BidLogStream.stream(files);
        if(randomLines!=null){
            // generate random indexes
            System.err.println("Generating all indexes");
            List<Integer> indexes = IntStream.range(0,randomLines).boxed().collect(Collectors.toList());
            System.err.println("Shuffle");
            Collections.shuffle(indexes);
            System.err.println("Take first N elements");
            indexes = indexes.subList(0,maxLines);
            System.err.println("Sort");
            indexes.sort(Integer::compare);
            int[] randomIndexes = indexes.stream().mapToInt(x-> x).toArray();

            // get random lines
            System.err.println("Get random lines");
            AtomicInteger indexer = new AtomicInteger();
            inputData = inputData.map(x -> Pair.of(indexer.incrementAndGet(),x))
                    .filter(x-> Arrays.binarySearch(randomIndexes,x.getKey()) >=0)
                    .map(Pair::getValue);
        }

        AppFilterData filterData = new AppFilterData(outputFile,outputFields);
        filterData.preProcess(outputFile);
        inputData
                .limit(maxLines)
                .map(FilteredBidEntry::new)
                .peek(FilteredBidEntry::postProcessEntry)
                .peek(filterData::addLabels)
                .peek(x -> filterData.progress())
                .forEach(filterData::printEntry);
        filterData.postProcess();
        filterData.printLabels();
    }

    public AppFilterData(String outputFile, String outputFields) {
        if(outputFile!=null){
            this.outputFile = Paths.get(outputFile);
        }
        if(outputFields!=null){
            fields = Arrays.stream(outputFields.split(",")).map(BidLogFields::valueOf).collect(Collectors.toList());
        }else {
            fields =  Arrays.stream(BidLogFields.values()).collect(Collectors.toList());
        }
        stats = new FilteredBidLabels(fields);
    }

    public void printEntry(FilteredBidEntry entry){
        PrintStream out = outputFile==null ? System.out : tempStream;

        String entryString = fields.stream()
                .map(field -> field.getValue(entry))
                .collect(Collectors.joining(","));

        out.println(entryString);
    }

    public void progress(){
        progress++;
        if(progress>=10_000){
            System.err.print(".");
            progress=0;
        }
    }

    public void addLabels(FilteredBidEntry entry){
        stats.addStatistic(entry);
    }

    public void printLabels(){
        System.err.println();
        System.err.println("-- ArffHeader ---");
        System.err.println(stats.getArffHeader());
        System.err.println("-----------------");
        System.err.println("----- Sizes -----");
        System.err.println(stats.getSizes());
        System.err.println("-----------------");
        System.err.println();
    }

    public void preProcess(String outputFile) throws IOException {
        if(outputFile != null){
            tempFile = File.createTempFile("dataprocessing-tempfile",".tmp");
            tempFile.deleteOnExit();
            tempStream = new PrintStream(tempFile,"UTF-8");
        }
    }

    public void postProcess() throws IOException {
        if(tempStream!=null){
            InputStream inStr = Files.newInputStream(tempFile.toPath());
            BufferedWriter outStr = Files.newBufferedWriter(outputFile, Charset.forName("UTF-8"));

            outStr.write(stats.getArffHeader());
            IOUtils.copy(inStr,outStr, Charset.forName("UTF-8"));

            IOUtils.closeQuietly(inStr,outStr,tempStream);
        }
        if(tempFile!= null){
            tempFile.delete();
        }
    }
}
