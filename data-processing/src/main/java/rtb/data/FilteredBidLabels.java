package rtb.data;

import lombok.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by daniel on 02.10.16.
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class FilteredBidLabels {
    private List<BidLogFields> fields;

    private Set<String> weekday = new HashSet<>();
    private Set<String> partOfDay = new HashSet<>();
    private Set<String> regionId = new HashSet<>();
    private Set<String> cityId = new HashSet<>();
    private Set<String> adExchange = new HashSet<>();
    private Set<String> domain = new HashSet<>();
    private Set<String> adSlotSize = new HashSet<>();
    private Set<String> adSlotVisibility = new HashSet<>();
    private Set<String> adSlotFormat = new HashSet<>();
    private Set<String> bidding = new HashSet<>();
    private Set<String> advertiserId = new HashSet<>();

    public FilteredBidLabels(List<BidLogFields> fields) {
        this.fields = fields;
    }

    public void addStatistic(FilteredBidEntry filteredBidEntry){
        fields.stream()
                .filter(field -> field != BidLogFields.TIMESTAMP)
                .forEach(field -> field.getLabels(this).add(field.getValue(filteredBidEntry)));
    }

    public String getSizes(){
        return fields.stream()
                .filter(x -> x != BidLogFields.TIMESTAMP)
                .map(field -> field.getPropertyName() + "\t" + field.getLabels(this).size())
                .collect(Collectors.joining("\n"));
    }

    public String getElements(){
        return fields.stream()
                .filter(x -> x != BidLogFields.TIMESTAMP)
                .map(field -> field.getPropertyName() + "\t" + field.getLabels(this).stream().collect(Collectors.joining(",")))
                .collect(Collectors.joining("\n"));
    }

    public String getArffHeader(){
        return "@RELATION rtb\n\n" +
                fields.stream()
                    .filter(x -> x != BidLogFields.TIMESTAMP)
                    .map(field -> "@ATTRIBUTE " + field.getPropertyName() + "\t{" + field.getLabels(this).stream().collect(Collectors.joining(",")) + "}")
                    .collect(Collectors.joining("\n"))
                + "\n\n@DATA\n";
    }
}
