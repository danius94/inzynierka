package rtb.data;

import lombok.Data;
import lombok.NoArgsConstructor;

import static org.apache.commons.lang3.StringUtils.trim;

/**
 * Created by daniel on 02.10.16.
 */
@Data
@NoArgsConstructor
public class BidLogEntry {
    private String bidId;
    private String timestamp;
    private String userId;
    private String userAgent;
    private String ipAddress;
    private String regionId;
    private String cityId;
    private String adExchange;
    private String domain;
    private String url;
    private String anonymousUrlId;
    private String adSlotId;
    private String adSlotWidth;
    private String adSlotHeight;
    private String adSlotVisibility;
    private String adSlotFormat;
    private String adSlotFloorPrice;
    private String creativeId;
    private String bidding;
    private String advertiserId;
    private String userProfile;

    public BidLogEntry(String[] logEntry){
        bidId = trim(getOrNull(logEntry,0));
        timestamp = trim(getOrNull(logEntry,1));
        userId = trim(getOrNull(logEntry,2));
        userAgent = trim(getOrNull(logEntry,3));
        ipAddress = trim(getOrNull(logEntry,4));
        regionId = trim(getOrNull(logEntry,5));
        cityId = trim(getOrNull(logEntry,6));
        adExchange = trim(getOrNull(logEntry,7));
        domain = trim(getOrNull(logEntry,8));
        url = trim(getOrNull(logEntry,9));
        anonymousUrlId = trim(getOrNull(logEntry,10));
        adSlotId = trim(getOrNull(logEntry,11));
        adSlotWidth = trim(getOrNull(logEntry,12));
        adSlotHeight = trim(getOrNull(logEntry,13));
        adSlotVisibility = trim(getOrNull(logEntry,14));
        adSlotFormat = trim(getOrNull(logEntry,15));
        adSlotFloorPrice = trim(getOrNull(logEntry,16));
        creativeId = trim(getOrNull(logEntry,17));
        bidding = trim(getOrNull(logEntry,18));
        advertiserId = trim(getOrNull(logEntry,19));
        userProfile = trim(getOrNull(logEntry,20));
    }

    private static <T> T getOrNull(T[] arr, int idx){
        return arr.length > idx ? arr[idx] : null;
    }
}
