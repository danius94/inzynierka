package rtb.data;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Stream;

import static org.apache.commons.collections4.ListUtils.emptyIfNull;

/**
 * Created by daniel on 02.10.16.
 */
public class BidLogStream {
    private static final String FIELD_SEPARATOR = "\t";
    private static final Charset CHARSET = StandardCharsets.UTF_8;

    private BidLogStream(){}

    public static Stream<BidLogEntry> stream(List<String> files){
        if(!canReadFiles(files)){
            throw new RuntimeException("Need read permission for all files");
        }
        return emptyIfNull(files).stream()
                .map(Paths::get)
                .map(file -> eatException(() -> Files.lines(file, CHARSET)))
                .reduce(Stream::concat)
                .orElse(Stream.<String>empty())
                .map(line -> line.split(FIELD_SEPARATOR))
                .map(BidLogEntry::new);
    }

    private static boolean canReadFiles(List<String> files){
        return emptyIfNull(files).stream()
                .map(Paths::get)
                .allMatch(Files::isReadable);
    }

    private static <T> T eatException(Callable<T> callable){
        try {
            return callable.call();
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
