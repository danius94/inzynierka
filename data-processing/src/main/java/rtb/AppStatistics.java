package rtb;

import org.apache.commons.lang3.tuple.Pair;
import rtb.data.BidLogEntry;
import rtb.data.BidLogStream;

import java.util.*;

/**
 * Created by daniel on 02.10.16.
 */
public class AppStatistics {
    ArrayList<Long> bids = new ArrayList<>();
    Map<Pair<String,String>,Long> size = new HashMap<>();
    int progress=0;


    public static void main(String[] args) {
        List<String> files = Arrays.asList(args);
        files.forEach(System.out::println);

        AppStatistics statistics = new AppStatistics();
        BidLogStream.stream(files)
                .peek(x -> statistics.progress())
                .forEach(statistics::addStatistics);

        statistics.printStatistics();
    }

    public void progress(){
        progress++;
        if(progress>=10_000){
            System.err.print(".");
            progress=0;
        }
    }

    public void addStatistics(BidLogEntry bidLogEntry){
        countBids(bidLogEntry);
        countAdSize(bidLogEntry);
    }

    public void printStatistics(){
        System.out.println();
        System.out.println();
        System.out.println("----- Bids -----");
        printBids();
        System.out.println();
        System.out.println();
        System.out.println("----- Size -----");
        printSize();
    }

    private void countBids(BidLogEntry bidLogEntry){
        int bid = Integer.parseInt(bidLogEntry.getBidding());
        while(bids.size()<=bid){
            bids.add(0L);
        }
        bids.set(bid, bids.get(bid)+1L);
    }

    private void printBids(){
        for(int i=0; i<bids.size(); i++){
            if(bids.get(i)>0L) {
                System.out.println(i + "\t\t" + bids.get(i));
            }
        }
    }

    private void countAdSize(BidLogEntry bidLogEntry){
        Pair<String,String> key = Pair.of(bidLogEntry.getAdSlotWidth(),bidLogEntry.getAdSlotHeight());
        Long count = size.getOrDefault(key, 0L);
        count++;
        size.put(key, count);
    }

    private void printSize() {
        size.entrySet().stream().forEach(e -> System.out.println(e.getKey().getLeft()+"x"+e.getKey().getRight()+"\t\t"+e.getValue()));
    }
}
