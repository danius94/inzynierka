grammar Rules;

rules: moaRule*;

moaRule: 'Rule' number=STRING ':' conditions '-->' value=STRING;

conditions: condition ('and' condition)* ;

condition: attribute=STRING '=' value=STRING;

STRING : [0-9a-zA-Z]+;

WS     : [ \n\t\r]+ -> skip;