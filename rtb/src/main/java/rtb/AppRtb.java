package rtb;

import rtb.data.InputDataProcessor;
import rtb.learner.Learner;
import rtb.rules.HMRRules;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by daniel on 27.11.16.
 */
public class AppRtb {

    public static void main(String[] args) throws IOException, InterruptedException {
        ServerSocket inputData = new ServerSocket(7708);
        ServerSocket intermediateServer = new ServerSocket(7709);
        ServerSocket outputRules = new ServerSocket(7710);

        InputDataProcessor inputDataProcessor = new InputDataProcessor(inputData, intermediateServer);
        Learner learner = new Learner();
        HMRRules hmrRules = new HMRRules(outputRules, learner.getClassifier());

        System.out.println("Initializing data processor");
        new Thread(inputDataProcessor).start();
        Thread.sleep(1000);
        System.out.println("Initializing learner");
        new Thread(learner).start();
        Thread.sleep(1000);
        System.out.println("Initializing rule parser");
        new Thread(hmrRules).start();
        Thread.sleep(1000);
        System.out.println("Ready.");
    }

}
