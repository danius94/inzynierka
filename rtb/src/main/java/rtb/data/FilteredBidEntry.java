package rtb.data;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

/**
 * Created by daniel on 02.10.16.
 */
@Data
public class FilteredBidEntry {
    public static final DateTimeFormatter DATE_FORMAT = new DateTimeFormatterBuilder().appendPattern("yyyyMMddHHmmss").appendValue(ChronoField.MILLI_OF_SECOND, 3).toFormatter();

    private String timestamp;
    private String weekday;
    private String partOfDay; // 22-6  7-11  12-14  15-17  18-22
    private String regionId;
    private String cityId;
    private String adExchange;
    private String domain;
    private String adSlotSize; // Width x Height
    private String adSlotVisibility;
    private String adSlotFormat;
    private String bidding;

    private FilteredBidEntry() {
    }

    public FilteredBidEntry(BidLogEntry bidLogEntry) {
        LocalDateTime date = LocalDateTime.parse(bidLogEntry.getTimestamp(), DATE_FORMAT);
        timestamp = date.toString();
        weekday = date.getDayOfWeek().name();
        partOfDay = getPartOfDay(date.getHour());
        regionId = bidLogEntry.getRegionId();
        cityId = bidLogEntry.getCityId();
        adExchange = bidLogEntry.getAdExchange();
        domain = bidLogEntry.getDomain();
        adSlotSize = bidLogEntry.getAdSlotWidth() + "x" + bidLogEntry.getAdSlotHeight();
        adSlotVisibility = bidLogEntry.getAdSlotVisibility();
        adSlotFormat = bidLogEntry.getAdSlotFormat();
        bidding = bidLogEntry.getBidding();
    }

    public void postProcessEntry() {
        if ("0".equals(adSlotFormat)) {
            adSlotFormat = "Na";
        } else if ("1".equals(adSlotFormat)) {
            adSlotFormat = "Fixed";
        } else if ("5".equals(adSlotFormat)) {
            adSlotFormat = "Pop";
        }

        if ("0".equals(adSlotVisibility)) {
            adSlotVisibility = "Na";
        } else if ("1".equals(adSlotVisibility)) {
            adSlotVisibility = "FirstView";
        } else if ("2".equals(adSlotVisibility)) {
            adSlotVisibility = "SecondView";
        } else if ("255".equals(adSlotVisibility)) {
            adSlotVisibility = "OtherView";
        }
    }

    private static String getPartOfDay(int hour) {
        // 22-6  7-11  12-14  15-17  18-22
        if (hour < 7) {
            return "22-6";
        } else if (hour < 12) {
            return "7-11";
        } else if (hour < 15) {
            return "12-14";
        } else if (hour < 18) {
            return "15-17";
        } else if (hour < 22) {
            return "18-22";
        } else {
            return "22-6";
        }
    }
}
