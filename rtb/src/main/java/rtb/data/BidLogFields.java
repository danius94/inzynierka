package rtb.data;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.text.WordUtils;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by daniel on 02.10.16.
 */
public enum BidLogFields {
    TIMESTAMP, WEEKDAY, PART_OF_DAY, REGION_ID, CITY_ID, AD_EXCHANGE, DOMAIN, AD_SLOT_SIZE, AD_SLOT_VISIBILITY, AD_SLOT_FORMAT, BIDDING;

    private final String propertyName = WordUtils.uncapitalize(WordUtils.capitalizeFully(this.name().replaceAll("_", " ")).replaceAll(" ", ""));

    public String getPropertyName() {
        return propertyName;
    }

    public String getValue(FilteredBidEntry entry) {
        try {
            return (String) PropertyUtils.getProperty(entry, propertyName);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

}
