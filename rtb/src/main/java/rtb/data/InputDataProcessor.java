package rtb.data;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by daniel on 27.11.16.
 */
public class InputDataProcessor implements Runnable {
    private static final String FIELD_SEPARATOR = "\t";

    private ServerSocket inputServerSocket;
    private ServerSocket outputServerSocket;

    public InputDataProcessor(ServerSocket inputServerSocket, ServerSocket outputServerSocket) {
        this.inputServerSocket = inputServerSocket;
        this.outputServerSocket = outputServerSocket;
    }

    @Override
    public void run() {
        try {
            PrintWriter printer = new PrintWriter(outputServerSocket.accept().getOutputStream());
            printer.println(getArffHeader());
            printer.flush();
            while (true) {
                try (Socket socket = inputServerSocket.accept()) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String userInput;
                    while ((userInput = reader.readLine()) != null) {
                        printer.println(convertToArff(userInput));
                        printer.flush();
                    }
                    IOUtils.closeQuietly(reader);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String convertToArff(String input) {
        if (StringUtils.isNotEmpty(input)) {
            String[] split = input.split(FIELD_SEPARATOR);
            BidLogEntry bidLogEntry = new BidLogEntry(split);
            FilteredBidEntry filteredBidEntry = new FilteredBidEntry(bidLogEntry);
            filteredBidEntry.postProcessEntry();
            return printEntry(filteredBidEntry);
        } else {
            return "";
        }
    }

    private String printEntry(FilteredBidEntry entry) {
        return Arrays.stream(BidLogFields.values()).filter(f -> f != BidLogFields.TIMESTAMP).map(field -> field.getValue(entry)).collect(Collectors.joining(","));
    }

    public String getArffHeader() {
        try {
            return IOUtils.toString(getClass().getResourceAsStream("/header"), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
