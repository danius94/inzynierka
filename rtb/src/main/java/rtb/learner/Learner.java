package rtb.learner;

import moa.classifiers.AbstractClassifier;
import moa.classifiers.core.driftdetection.ADWINChangeDetector;
import moa.classifiers.drift.DriftDetectionMethodClassifier;
import moa.classifiers.rules.RuleClassifierNBayes;
import moa.core.InstanceExample;
import moa.streams.ArffFileStream;
import rtb.moa.ArffSocketStream;

/**
 * Created by daniel on 27.11.16.
 */
public class Learner implements Runnable {

    private final AbstractClassifier classifier;
    private ArffFileStream instanceStream;

    public Learner(AbstractClassifier classifier, ArffFileStream instanceStream) {
        this.classifier = classifier;
        this.instanceStream = instanceStream;
    }

    public Learner() {
        DriftDetectionMethodClassifier classifier = new DriftDetectionMethodClassifier();
        classifier.baseLearnerOption.setCurrentObject(new RuleClassifierNBayes());
        classifier.driftDetectionMethodOption.setCurrentObject(new ADWINChangeDetector());

        this.classifier = classifier;
        this.instanceStream = new ArffSocketStream();

    }

    @Override
    public void run() {
        instanceStream.prepareForUse();
        classifier.setModelContext(instanceStream.getHeader());
        classifier.prepareForUse();

        while (true) {
            InstanceExample instance = instanceStream.nextInstance();
            synchronized (classifier) {
                classifier.trainOnInstance(instance);
            }
        }
    }

    public AbstractClassifier getClassifier() {
        return classifier;
    }
}
