package rtb.utils;

import java.util.List;
import java.util.ListIterator;

public class ReversedListIterator<T> implements ListIterator<T> {

    private final ListIterator<T> listIterator;

    public ReversedListIterator(List<T> list) {
        this.listIterator = list.listIterator(list.size());
    }

    public static <R> ReversedListIterator<R> reversedIterator(List<R> list){
        return new ReversedListIterator<>(list);
    }

    @Override
    public boolean hasNext() {
        return listIterator.hasPrevious();
    }

    @Override
    public T next() {
        return listIterator.previous();
    }

    @Override
    public boolean hasPrevious() {
        return listIterator.hasNext();
    }

    @Override
    public T previous() {
        return listIterator.next();
    }

    @Override
    public int nextIndex() {
        return listIterator.previousIndex();
    }

    @Override
    public int previousIndex() {
        return listIterator.nextIndex();
    }

    @Override
    public void remove() {
        listIterator.remove();
    }

    @Override
    public void set(T o) {
        listIterator.set(o);
    }

    @Override
    public void add(T o) {
        listIterator.add(o);
    }
}
