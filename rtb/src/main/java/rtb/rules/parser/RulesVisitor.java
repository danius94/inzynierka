// Generated from /home/daniel/Desktop/dev/inzynierka/learner/src/main/resources/Rules.g4 by ANTLR 4.5.3
package rtb.rules.parser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link RulesParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface RulesVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link RulesParser#rules}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRules(RulesParser.RulesContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#moaRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMoaRule(RulesParser.MoaRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#conditions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConditions(RulesParser.ConditionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition(RulesParser.ConditionContext ctx);
}