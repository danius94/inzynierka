package rtb.rules.parser;

import rtb.data.BidLogFields;

import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Daniel Tyka on 1/1/17.
 */
public class HMRRulesSchemaGenerator {

    private static final String IDENT = "\n      ";
    private static final String SCHEMA_NAME = "rtb";
    private static final String DECISION_ATTRIBUTE = BidLogFields.BIDDING.getPropertyName();
    /*private static final List<String> CONDITION_ATTRINUTES = Arrays.stream(BidLogFields.values())
            .filter(x -> x != BidLogFields.TIMESTAMP)
            .filter(x -> x != BidLogFields.BIDDING)
            .map(BidLogFields::getPropertyName)
            .collect(Collectors.toList());*/

    private List<String> usedConditionAttributes = new ArrayList<>();
    private Map<String,List<String>> values = new HashMap<>();

    public void printSchemaDefinition(RulesParser.RulesContext input, PrintWriter output){
        analyzeInput(input);
        output.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TYPES DEFINITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%\n");
        values.keySet().forEach(a -> printType(a,output));
        output.println("%%%%%%%%%%%%%%%%%%%%%%%%% ATTRIBUTES DEFINITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%\n");
        values.keySet().forEach(a -> printAttribute(a,output));
        output.println("%%%%%%%%%%%%%%%%%%%%%%%% TABLE SCHEMAS DEFINITIONS %%%%%%%%%%%%%%%%%%%%%%%%\n");
        printSchema(output);
        output.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%% RULES DEFINITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
    }

    private void printSchema(PrintWriter output){
        output.println("xschm " + SCHEMA_NAME + ": [" + usedConditionAttributes.stream().collect(Collectors.joining(",")) + "] ==> [" + DECISION_ATTRIBUTE + "].\n");
    }

    private void printAttribute(String attribute, PrintWriter output){
        String comm = DECISION_ATTRIBUTE.equals(attribute) ? "out" : "in";
        output.println("xattr [name: "+attribute+","+IDENT
                +" class: simple,"+IDENT
                +" type: "+attribute+","+ IDENT
                +" comm: "+comm+ IDENT
                +"].\n");
    }

    private void printType(String attribute, PrintWriter output){
        output.println("xtype [name: "+attribute+","+IDENT
                +" base: symbolic,"+IDENT
                +" domain: ["+values.get(attribute).stream().collect(Collectors.joining(","))+"]"+ IDENT
                +"].\n");
    }

    private void analyzeInput(RulesParser.RulesContext input){
        for(RulesParser.MoaRuleContext moaRuleContext : input.moaRule()){
            RulesParser.ConditionsContext conditionsContext = moaRuleContext.conditions();
            for(RulesParser.ConditionContext conditionContext : conditionsContext.condition()){
                usedConditionAttributes.add(conditionContext.attribute.getText());
                putValue(conditionContext.attribute.getText(),conditionContext.value.getText());
            }
            putValue(DECISION_ATTRIBUTE, moaRuleContext.value.getText());
        }
    }

    private void putValue(String attribute, String value){
        List<String> vals = values.get(attribute);
        if(vals == null){
            vals = new LinkedList<>();
            values.put(attribute,vals);
        }
        vals.add(value);
    }
}
