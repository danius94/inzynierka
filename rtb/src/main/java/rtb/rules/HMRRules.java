package rtb.rules;

import moa.classifiers.AbstractClassifier;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import rtb.data.BidLogFields;
import rtb.rules.parser.HMRRulesSchemaGenerator;
import rtb.rules.parser.RulesLexer;
import rtb.rules.parser.RulesParser;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.substringBetween;

/**
 * Created by Daniel Tyka on 2016-11-15.
 */
public class HMRRules implements Runnable {

    private final ServerSocket outputRules;
    private final AbstractClassifier classifier;

    public HMRRules(ServerSocket outputRules, AbstractClassifier classifier) {
        this.outputRules = outputRules;
        this.classifier = classifier;
    }

    @Override
    public void run() {
        while (true) {
            try (Socket socket = outputRules.accept()) {
                PrintWriter output = new PrintWriter(socket.getOutputStream());
                StringBuilder desc = new StringBuilder();
                synchronized (classifier) {
                    classifier.getDescription(desc, 0);
                }
                printRules(filterRules(desc.toString()),output);
                output.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String filterRules(String input){
        return Arrays.stream(input.split("\n")).filter(s -> s.startsWith("Rule")).collect(Collectors.joining("\n"));
    }

    private void translateRules(RulesParser.RulesContext input, PrintWriter output){
        HMRRulesConverter hmrRulesConverter = new HMRRulesConverter(output);
        try{
            hmrRulesConverter.visitRules(input);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void schemaDefinition(RulesParser.RulesContext input, PrintWriter output){
        HMRRulesSchemaGenerator schemaGenerator = new HMRRulesSchemaGenerator();
        schemaGenerator.printSchemaDefinition(input,output);
    }

    private void printRules(String input, PrintWriter output) {
        RulesLexer lexer = new RulesLexer(new ANTLRInputStream(input));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        RulesParser parser = new RulesParser(tokens);
        parser.setBuildParseTree(true);
        RulesParser.RulesContext rules = parser.rules();

        schemaDefinition(rules,output);
        translateRules(rules, output);
    }

    public static void main(String[] args) {
        String input = "Rule 11: nominal5 = value3 and nominal3 = value4  --> class1\n" +
                "\n" +
                "Rule 12: nominal2 = value5 --> class2";
        PrintWriter printWriter = new PrintWriter(System.out);
        HMRRules hmrRules = new HMRRules(null,null);
        hmrRules.printRules(input,printWriter);
        printWriter.flush();

    }
}
