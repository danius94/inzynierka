package rtb.rules;

import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import rtb.rules.parser.RulesBaseVisitor;
import rtb.rules.parser.RulesParser;
import rtb.utils.ReversedListIterator;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.stream.IntStream;

public class HMRRulesConverter extends RulesBaseVisitor<HMRRulesConverter> {
    private static final String INDENTATION = "  ";
    private int indent = 0;
    private PrintWriter result;
    private int ruleCount = 0;

    public HMRRulesConverter(PrintWriter result) {
        this.result = result;
    }

    private HMRRulesConverter indent() {
        append("\n");
        IntStream.range(0, indent).forEach(x -> append(INDENTATION));
        return this;
    }

    private HMRRulesConverter incIndent() {
        indent++;
        return this;
    }

    private HMRRulesConverter decIndent() {
        indent--;
        return this;
    }

    private HMRRulesConverter append(CharSequence charSequence) {
        result.append(charSequence);
        return this;
    }

    @Override
    public HMRRulesConverter visitRules(RulesParser.RulesContext ctx) {
        return visitGenericList(this,ReversedListIterator.reversedIterator(ctx.moaRule()),this::visitMoaRule,this::indent);
    }

    @Override
    public HMRRulesConverter visitMoaRule(RulesParser.MoaRuleContext ctx) {
        ruleCount++;
        append("xrule rtb/");
        append(Integer.toString(ruleCount));
        append(":");
        incIndent();
        if(ctx.conditions() != null && !ctx.conditions().isEmpty()){
            incIndent();
            indent();
            append("[");
            visitConditions(ctx.conditions());
            append("]");
            decIndent();
        }
        indent();
        append("==>");
        incIndent();
        indent();
        append("[bidding set ");
        append(ctx.value.getText());
        append("].");

        decIndent();
        decIndent();
        indent();
        return this;
    }

    @Override
    public HMRRulesConverter visitConditions(RulesParser.ConditionsContext ctx) {
        return visitGenericList(this,ctx.condition().iterator(),this::visitCondition,()->{append(",");indent();append(" ");});
    }

    @Override
    public HMRRulesConverter visitCondition(RulesParser.ConditionContext ctx) {
        return append(ctx.attribute.getText())
                .append(" eq ")
                .append(ctx.value.getText());
    }

    @Override
    public HMRRulesConverter visitTerminal(TerminalNode terminalNode) {
        return append(terminalNode.getText());
    }

    @Override
    public HMRRulesConverter visitErrorNode(ErrorNode errorNode) {
        throw new RuntimeException("Error: " + errorNode.getPayload());
    }

    private static <T> HMRRulesConverter visitGenericList(HMRRulesConverter v, Iterator<T> context, Consumer<T> consumer, Runnable separator) {
        boolean first = true;
        while(context.hasNext()) {
            T t = context.next();
            if (!first && separator != null) {
                separator.run();
            }
            first = false;
            consumer.accept(t);
        }
        return v;
    }

}
