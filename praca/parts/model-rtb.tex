\chapter{Model zautomatyzowanego zakupu powierzchni reklamowej}
\label{cha:model-rtb}
%------------------------------------------

Marketing w~internecie jest aktualnie jedną z~najszybciej rozwijających się branży w~przemyśle informatycznym. Jego obroty co roku na~całym świecie wzrastają średnio od~10\% do~20\%. W~samym USA w~2015 roku dochód z~reklam internetowych wyniósł 59,6 miliardów dolarów~\cite{rtb-advertising-raport}. W~2013 roku dochód z~reklam internetowych przewyższył dochód z~reklam w~,,tradycyjnych'' mediach, a~w kolejnych latach różnica ta tylko wzrastała.

W~branży reklam online jedną z~ważniejszych koncepcji powstałych w~ciągu ostatnich lat były \textbf{aukcje w~czasie rzeczywistym} (\textit{ang. RTB -- real-time bidding}). W~\textit{RTB} licytuje się każde pojedyncze wyświetlenie reklamy, a~cała aukcja trwa 100$ms$. Dzięki temu reklamodawcy mają możliwość decydowania o~każdym wyświetleniu reklamy i~optymalnego zarządzania swoim budżetem. \textit{RTB} fundamentalnie zmieniło obraz rynku cyfrowych mediów poprzez~zautomatyzowanie procesu zakupowego oraz~objęcie swoim zasięgiem dużej liczby dostępnych powierzchni reklamowych oferowanych przez wydawców (dotychczas rozproszonych na~pomniejsze sieci). Kładzie również nacisk na~optymalizację kosztów i~zysków bazującą na~danych o~użytkowniku oraz~jego zachowaniu~\cite{rtb-general, rtb-analysis}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\textwidth]{diagramy/out/rtb-diagram.pdf}
    
    \caption{\label{fig:rtb-diagram} Schemat poglądowy przedstawiający ekosystem reklam online, jego główne części składowe oraz~możliwe interakcje między nimi~\cite{rtb-general}}
\end{figure}

Przed pojawieniem się aukcji czasu rzeczywistego w~2009 roku, rynek reklam internetowych zdominowany był przede wszystkim przez \textbf{indywidualne umowy} z~wydawcami oraz~\textbf{sieci reklamowe} (\textit{ang. ad network})~\cite{rtb-analysis}. W~tych pierwszych reklamodawcy i~wydawcy indywidualnie negocjują cenę i~warunki kontraktu. Wydawcy zwykle gwarantują konkretną liczbę wyświetleń reklam na~stronie, niezależnie od~innych czynników jak~zainteresowania użytkownika oglądającego reklamę, godzina wyświetlenia itd. O~wyborze danego wydawcy decyduje często jego reputacja oraz~profil przeciętnego użytkownika serwisu. Sieci reklamowe powstały w~odpowiedzi na~rosnącą liczbę stron WWW (a więc potencjalnych miejsc na~reklamy) w~czasie rewolucji internetowej. Poszczególne sieci reklamowe mogą się znacznie różnić między sobą, ale~podstawowym ich celem jest pośredniczenie w~sprzedaży miejsca reklamowego (zwykle poprzez~organizowanie aukcji). Wydawcy  rejestrują się w~takiej sieci, oferując miejsce reklamowe, a~reklamodawcy licytują się o~nie. Niektóre sieci prowadzą kampanie reklamowe w~imieniu reklamodawców, bazując na~ustalonych z~nimi celach i~zasadach. 

W~miarę pojawiania się kolejnych sieci reklamowych, w~niektórych z~nich narastał problem nadmiernej podaży, a~w innych nadmiernego popytu. Reklamodawcy i~wydawcy, chcąc dostosowywać się do~zmiennych warunków, musieli rejestrować się w~wielu sieciach naraz w~poszukiwaniu najlepszych dla siebie ofert. Okazało się to trudne i~nieefektywne na~dłuższą metę -- co stanowiło barierę dla nowych użytkowników sieci. W~odpowiedzi na~te problemy powstali \textbf{pośrednicy reklam} (\textit{ang. AdX -- ad exchange}). Ich zadaniem było połączenie ze~sobą setek mniejszych sieci, aby~wyrównać popyt i~podaż oraz~unormować rynek reklam \textit{online}~\cite{rtb-analysis}. Wraz z~powstaniem pośredników reklam rozwinęły się \textbf{platformy popytowe} (\textit{ang. DSP -- demand side platform}) i~\textbf{podażowe} (\textit{ang. SSP -- supply side platform}), które~pomagają swoim klientom -- odpowiednio reklamodawcom i~wydawcom -- zarządzać ich zasobami (budżetem lub~miejscem reklamowym), optymalizować strategie, bazując na~analizie dużej ilości danych i~uczestniczyć w~aukcjach. Systemy te pozwalają znaleźć najlepsze oferty w~prosty, wygodny i~ujednolicony sposób. Powstali również \textbf{pośrednicy danych} (\textit{ang. DX -- data exchange} lub~\textit{DMP -- data management platform}), którzy zbierają i~przechowują informacje o~użytkownikach oraz~serwują te dane uczestnikom całego systemu reklam internetowych. Dostęp do~takich danych umożliwił dalszą optymalizację kosztów i~zysków bazującą na~informacjach o~użytkowniku oraz~personalizację reklam~\cite{rtb-survey}. Schemat poglądowy opisanego ekosystemu reklam online znajduje się na~rysunku \ref{fig:rtb-diagram}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\textwidth]{diagramy/out/rtb-how-it-works.pdf}
    
    \caption{\label{fig:rtb-how-it-works} Schemat przedstawiający jak~działają aukcje w~czasie rzeczywistym~\cite{rtb-general}}
\end{figure}

Jednym z~najważniejszych usprawnień wprowadzonych przez pośredników reklam były właśnie \textbf{aukcje czasu rzeczywistego}. Przebieg takiej aukcji jest zobrazowany na~rysunku \ref{fig:rtb-how-it-works} i~składa się z~następujących kroków:

\begin{enumerate}
\item użytkownik odwiedza stronę WWW z wolnym miejscem reklamowym;
\item w trakcie ładowania strony, wydawca (a dokładniej \textit{SSP}) wysyła zapytanie o reklamę do pośrednika reklam;
\item pośrednik reklam otwiera nową aukcję i informuje o tym fakcie reklamodawców (\textit{DSP});
\item \textit{DSP} może ściągnąć informacje o użytkowniku od pośrednika danych;
\item jeśli \textit{DSP} uzna, że chce uczestniczyć w aukcji, wysyła licytowaną kwotę do pośrednika reklam;
\item pośrednik reklam informuje zwycięzcę aukcji o wygranej;
\item do użytkownika przesyłana jest reklama;
\item jeśli nastąpi kliknięcie reklamy lub konwersja\footnote{Konwersja jest to zamiana użytkownika strony w klienta, czyli cel, który chce osiągnąć reklamodawca; przykładowo: sprzedaż produktu, subskrypcja usługi lub wypełnienie ankiety} -- \textit{DSP} jest o tym fakcie informowany.
\end{enumerate}

Warto tu zaznaczyć, że~aukcjonariusze nie są informowani o~tym, ile zalicytowali inni uczestnicy aukcji. Cały proces trwa około 100$ms$, a~takich aukcji w~ciągu sekundy może być nawet kilka milionów~\cite{rtb-general}, więc algorytmy używane przez \textit{DSP} muszą być dobrze zoptymalizowane, nie tylko po~to, aby~zmieścić się w~limicie czasowym, ale~też obsłużyć ogromne liczby takich procesów jednocześnie. Same aukcje są zwykle prowadzone w~systemie drugiej ceny (system Vickreya). Oznacza to, że~aukcję wygrywa reklamodawca, który~zaoferował najwyższą kwotę, ale~płaci tyle, ile zaoferował reklamodawca drugi z~kolei. Optymalną strategią reklamodawcy w~takim systemie jest oferowanie faktycznej wartości miejsca na~reklamę~\cite{rtb-general} -- co stało się głównym powodem jego stosowania. W~ten sposób niwelowany jest problem zawyżania i~zaniżania cen oraz~normalizuje się rynek.

Powstanie \textit{RTB} otworzyło nowe ścieżki do~badań naukowych w~dziedzinach takich jak~\textit{machine learning} i~\textit{data mining}. Automatyzacja aukcji zrodziła potrzebę stworzenia optymalnej strategii do~licytacji. Taka strategia musi brać pod~uwagę przede wszystkim profil użytkownika, ale~też specyfikę reklamodawcy, założone cele, dostępny budżet, informacje o~miejscu na~reklamę, informację zwrotną od~użytkownika (kliknięcie, konwersja) itd. Po~przeprocesowaniu tych informacji musi podjąć decyzję czy~wziąć udział w~aukcji i~jaka kwotę postawić. Oczywiście musi też zmieścić się w~limicie czasowym i~bardzo szybko przetwarzać kolejne dane. Podejścia do~stworzenia takiej strategii są różne, ale~zwykle opierają się w~jakiś sposób na~oszacowaniu szansy na~wygranie aukcji (w zależności od~postawionej kwoty) i~przewidywaniu reakcji użytkownika (szansy na~kliknięcie i~konwersję)~\cite{rtb-predict-win-price, rtb-general}. Niniejsza praca podejmuje temat oszacowania szansy na~wygranie aukcji.