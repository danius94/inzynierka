\chapter{Specyfikacja wymagań}
\label{cha:specyfikacja-wymagan}
%------------------------------------------

Zautomatyzowane aukcje czasu rzeczywistego (\textit{RTB}) to stosunkowo nowy i~interesujący temat, który~nie zebrał jeszcze wystarczającej uwagi w~świecie nauki. Jak~wspomniano w~rozdziale~\ref{cha:model-rtb}, głównym problemem w~\textit{RTB} jest opracowanie optymalnej strategii udziału w~licytacjach. Jej efektywne działanie jest kluczowe zarówno dla reklamodawców, jak~i~wydawców. Taka strategia musi uwzględniać wiele różnych czynników. Ostateczna decyzja, czy~wziąć udział w~danej aukcji i~jaką kwotę zalicytować, zwykle podejmowana jest na~podstawie przewidywanej reakcji użytkownika (np. prawdopodobieństwo kliknięcia reklamy) oraz~szacowaną szansę na~wygraną danej aukcji (w zależności od~postawionej kwoty). Takie strategie istnieją i~spełniają swoje zadanie, ale~nadal wymagają usprawnień.

Badania przeprowadzone przez \citeauthor{rtb-analysis}~\cite{rtb-analysis} w~\citeyear{rtb-analysis} roku pokazują problemy w~obecnych strategiach. Wskazano, że~problemy takie jak~szacowanie minimalnej ceny sprzedaży, dostosowywanie się do~tymczasowych zmian oraz~aktywność użytkowników w~ciągu dnia nie otrzymały należnej uwagi. Odkryto tendencję, że~w~godzinach wczesnoporannych liczba aukcjonariuszy znacząco przewyższa liczbę dostępnych miejsc reklamowych, a~po południu jest na~odwrót. Przedstawiono to na~rysunku~\ref{fig:req-rtb-analysis}. O~świcie, kiedy podaż jest niska (użytkownicy zwykle śpią), aukcjonariusze mimo wszystko starają się licytować o~miejsce reklamowe. Podaż jest niska, a~konkurencja wysoka, więc ceny są duże. Wysokie ceny prowadzą do~szybkiego wyczerpania dziennego budżetu\footnote{Reklamodawcy zwykle mają wyznaczony budżet na~całą kampanię reklamową i~chcą go rozłożyć równomiernie na~cały okres jej trwania. Dlatego wyznaczają dzienny limit budżetu.}. Wskutek czego później, w~ciągu dnia, gdy jest duża podaż (wysoka aktywność użytkowników), jest mało aukcjonariuszy, więc i~ceny są niskie. Ten wzrost i~spadek cen wcale nie idzie w~parze z~jakością\footnote{rozumianej tu jako ilość kliknięć i~konwersji} miejsca reklamowego. Takie zachowanie jest nieefektywne i~pozbawione sensu.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\textwidth]{img/req-rtb-analysis.png}
	
    \caption[Rozkład liczby aukcjonariuszy i~wyświetleń reklam w~ciągu dnia~\cite{rtb-analysis}]{\label{fig:req-rtb-analysis} Rozkład liczby aukcjonariuszy i~wyświetleń reklam w~ciągu dnia. Widać niezbalansowanie popytu i~podaży w~pewnych godzinach~\cite{rtb-analysis}.}
\end{figure}

Celem pracy jest opracowanie fragmentu strategii, która~będzie starała się częściowo zaradzić powyższym problemom. Stworzony system, na~podstawie logów z~przebiegu poprzednich aukcji, w~czasie rzeczywistym będzie generował reguły przewidujące, ile zalicytują inni aukcjonariusze.

Biorąc pod~uwagę wyznaczony cel pracy, powyższe problemy, mechanikę \textit{RTB} (opisaną w~rozdziale~\ref{cha:model-rtb}) oraz~specyfikę uczenia maszynowego ze~strumienia danych (poruszoną w~rozdziale~\ref{cha:metody-uczenia-maszynowego}) wyspecyfikowano następujące wymagania do~systemu:

\begin{enumerate}
\item przewidywanie licytowanej kwoty przez innych aukcjonariuszy;
\item działanie na strumieniu danych (potencjalnie nieskończenie dużym);
\item aktualizowanie modelu na bieżąco, na podstawie przychodzących danych;
\item generowanie reguł na żądanie, w dowolnym momencie;
\item generowani reguł w określonym formacie\footnote{Wybrano format HMR, który będzie opisany w sekcji~\ref{sec:notacja-hmr}.}, które zostaną wykorzystane przez system wnioskujący;
\item dostosowywanie się do zmian w danych -- możliwość występowania \textit{concept driftu}.
\end{enumerate}

Wybór reguł do~reprezentacji wiedzy podyktowany był ich efektywnym i~deterministycznym działaniem oraz~czytelnością dla człowieka. Dzięki tym cechom znacznie ułatwiona będzie ,,ręczna'' analiza działania algorytmu, przeprowadzanie audytów systemu czy~też zrozumienie obecnych trendów na~rynku reklam. Ponadto specyfika reguł pozwala bezproblemowo śledzić proces rozumowania i~prezentować użytkownikowi pełne objaśnienie podjętej decyzji. Ta własność -- \textit{intelligibility} -- jest jednym z~ważniejszych wymagań stawianych systemom sztucznej inteligencji. Co więcej, rozporządzenie\footnote{Zobacz: artykuł 71 rozporządzenia parlamentu europejskiego w~sprawie ochrony osób fizycznych w~związku z~przetwarzaniem danych osobowych, dostępnego pod~adresem:\\\url{http://eur-lex.europa.eu/legal-content/PL/TXT/PDF/?uri=CELEX:32016R0679&from=EN}} Parlamentu Europejskiego z~27 kwietnia 2016 r. będzie od~nakładać obowiązek na~każde oprogramowanie sztucznej inteligencji, które~przetwarza dane osobowe, aby~umożliwiało użytkownikowi uzyskanie wyjaśnienia decyzji o~użytkowniku, podjętej przez system.

Proponowanym podejściem jest wykorzystanie klasyfikacji inkrementacyjnej opartej o~reguły, gdzie klasą będzie licytowana kwota.
