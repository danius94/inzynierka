\chapter{Metody uczenia maszynowego}
\label{cha:metody-uczenia-maszynowego}
%------------------------------------------

\textbf{Uczenie maszynowe} (\textit{ang. machine learning}) to dziedzina informatyki, która powstała w wyniku rozwoju badań nad sztuczną inteligencją. Głównym jej celem jest tworzenie i badanie systemów, które są w stanie uczyć się, odkrywać wzorce, podejmować decyzje oraz samodoskonalić się na podstawie nabytych doświadczeń (czyli otrzymywanych danych). Uczenie maszynowe często stosowane jest do rozwiązywania problemów, dla których stworzenie ,,tradycyjnego'' algorytmu byłoby trudne, niewydajne, czasochłonne, a nawet niemożliwe.

Uczenie maszynowe polega na~doborze odpowiednich cech, na~podstawie których zbudowany zostanie odpowiedni model do~rozwiązania danego zadania~\cite{ml-flach}. \textbf{Zadanie} (\textit{ang. task}) w~uczeniu maszynowym rozumie się jako pewną abstrakcyjną reprezentację problemu, który~można rozwiązać za~pomocą uczenia maszynowego. Konkretny problem opisywany jest przez $n$ \textbf{atrybutów} (\textit{ang. features}). Każdy z~nich ma określony zakres wartości -- najczęściej dyskretny (zbiór) lub~ciągły (liczba), ale~też inne np. tekst i~data. Taki zbiór atrybutów wraz z~ich możliwymi wartościami definiuje ,,język'' opisujący wszystkie możliwe obiekty w~danym problemie. Większość zadań polega na~przypisaniu odpowiedniego wyjścia dla danego wejścia. Takie przypisanie realizowane jest przez \textbf{model} -- a~więc rozwiązanie zadania polega na~stworzeniu odpowiedniego modelu. \textbf{Algorytm uczący} buduje model poprzez~analizowanie \textbf{zbioru uczącego} (\textit{ang. training set})~\cite{ml-flach}. Schematy, przedstawiające jak~uczenie maszynowe jest używane do~rozwiązywania różnych zadań, znajdują się na~rysunkach \ref{fig:machine-learning-predictive} i~\ref{fig:machine-learning-descriptive}.

Zadania dzielą się na~predykcyjne i~opisowe. Predykcyjne to takie, w~których na~podstawie podanych atrybutów obiektu trzeba przewidzieć dla niego wartość jednego docelowego atrybutu. Są one najczęściej występującymi zadaniami uczenia maszynowego. Należą do~nich między innymi zadania klasyfikacji, regresji i~klasteryzacji (opisane poniżej). Natomiast zadania opisowe polegają na~wydobyciu ukrytych zależności w~danych. Jest to np. zadanie odkrycia reguł asocjacyjnych (również opisane poniżej).

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.75\textwidth]{diagramy/out/rtb-machine-learning-predictive.pdf}
	
    \caption{\label{fig:machine-learning-predictive} Schemat przedstawiający zadania predykcyjne~\cite{ml-flach}}
\end{figure}


\begin{figure}[ht]
    \centering
    \includegraphics[width=0.75\textwidth]{diagramy/out/rtb-machine-learning-descriptive.pdf}
	
    \caption{\label{fig:machine-learning-descriptive} Schemat przedstawiający zadania opisowe~\cite{ml-flach}}
\end{figure}

Algorytmy dzielą się na~kilka kategorii, m.in. \textbf{uczenie nadzorowane} (\textit{ang. supervised learning}) i~\textbf{nienadzorowane} (\textit{ang. unsupervised learning}). W~uczeniu nadzorowanym dane uczące same w~sobie zawierają pożądaną odpowiedź, a~zadaniem algorytmu jest odkrycie wzorca, według którego~będzie otrzymać oczekiwaną odpowiedź. Uczenie nadzorowane polega więc na~uczeniu się przez przykład. Algorytm musi generalizować zdobywaną wiedzę  (w ,,rozsądnym'' stopniu), tak żeby stworzony model mógł poprawnie przewidywać odpowiedź dla nowych, wcześniej niewidzianych przykładów. Wyróżnia się przede wszystkim dwa rodzaje uczenia nadzorowanego: \textbf{klasyfikacja} (\textit{ang. classification}) i~\textbf{regresja} (\textit{ang. regression})~\cite{ml-flach}.

Klasyfikacja to jeden z~najbardziej podstawowych i~najczęściej występujących problemów uczenia maszynowego. Z~tego powodu powstało wiele różnych algorytmów z~zupełnie odmiennymi podejściami do~zadania. Przykładowo istnieją klasyfikatory oparte o~drzewa decyzyjne, Bayesowskie, liniowe, logiki rozmytej i~regułowe. W~klasyfikacji zakłada się, że~atrybut docelowy (często nazywany \textit{klasą}) jest dyskretny i~zwykle ma tylko kilka możliwych wartości. Ze~względu na~liczebność zbioru klas wyróżnia się klasyfikatory binarne (dokładnie 2 klasy) i~wieloklasowe (\textit{ang. multiclass, multinomial}), ponieważ~niektóre prostsze algorytmy są w~stanie obsłużyć tylko 2 klasy. Sztandarowym przykładem zadania klasyfikacji jest wykrywanie niechcianych wiadomości i~rozpoznawanie chorób na~podstawie symptomów.

Regresja jest problemem podobnym do~klasyfikacji z~taką różnicą, że~atrybuty docelowe w~danych są liczbami rzeczywistymi. Zadanie polega na~odkryciu pewnej funkcji, która~przekształca cechy uczące w~liczbę rzeczywistą. Typowym zadaniem w~regresji jest dopasowanie funkcji do~danych wejściowych.

Skuteczność algorytmów uczenia nadzorowanego łatwo zmierzyć. Dla klasyfikacji najczęściej jest to procentowa trafność predykcji klasy, a~dla regresji może być to np. błąd średniokwadratowy pomiędzy oczekiwanym wyjściem a~rzeczywistym wyjściem.

W~uczeniu \textbf{nienadzorowanym} w~przeciwieństwie do~nadzorowanego algorytm nie otrzymuje na~wejściu oczekiwanych odpowiedzi, ani nawet ich przybliżenia. Sam musi on odkryć pewne zależności w~danych. Z~tego powodu ciężko jest zdefiniować jego skuteczność. Rodzaje uczenia nienadzorowanego to przede wszystkim: \textbf{klasteryzacja} (\textit{ang. clustering}) i~\textbf{reguły asocjacyjne} (\textit{ang. association rules}).

Klasteryzacja przypomina trochę problem klasyfikacji z~taką różnicą, że~na wejściu nie ma podanych etykiet dla danych. Algorytm sam musi podzielić dane na~grupy o~podobnych cechach. Samo podobieństwo jest już określane dla konkretnego problemu. Na~przykład mogłoby zostać zdefiniowane jako liczba zgodnych ze~sobą atrybutów w~przykładach uczących albo~prosta odległość euklidesowa. Warto dodać, że~zwykle nie jest znana liczba grup, na~które trzeba podzielić dane, więc algorytm też musi to odkryć. Przykładowym zadaniem byłaby systematyka organizmów żywych lub~wykrywanie podejrzanej aktywności u~użytkowników jakiegoś serwisu -- czyli potencjalnego oszustwa.

Zadanie reguł asocjacjacyjnych polega na~odszukaniu grup atrybutów, które~często występują razem. Zwykle używane są one w~sklepach internetowych do~polecania produktów swoim klientom. Z~transakcji z~przeszłości wyszukiwane są takie grupy produktów i~na ich podstawie powstaje w~sklepie sekcja ,,inni kupili też''.

\begin{table}[h]
    \centering
    
    \begin{threeparttable}
        \caption{Podsumowanie rodzajów zadań i~algorytmów}
        \label{tab:machine-learning-algorithm-x-task}
		
        \begin{tabularx}{0.8\textwidth}{l|l|l}
		
                                        & \textsc{Zadania predykcyjne}    & \textsc{Zadania opisowe}    \\ \hline
        \textsc{Uczenie nadzorowane}    & klasyfikacja, regresja & odkrywanie podgrup\tnote{*} \\ \hline
        \textsc{Uczenie nienadzorowane} & klasteryzacja          & reguły asocjacyjne          \\ \hline
		
        \end{tabularx}
		
        \begin{tablenotes}
            \footnotesize
            \item[*] nieopisane w~pracy; podano jako przykład
        \end{tablenotes}
		
    \end{threeparttable}
\end{table}


Kolejnym podziałem metod uczenia maszynowego jest podział na~metody batchowe i~strumieniowe.

%------------------------------------------
\section{Metody batchowe}
\label{sec:metody-batchowe}
%------------------------------------------

Wśród algorytmów uczenia maszynowego rozwijanych w~ciągu ostatnich kilkudziesięciu lat, prym wiodły przede wszystkim standardowe metody batchowe, skupiające się na~małych zbiorach danych i~statycznym środowisku~\cite{ml-gama}. W~metodach batchowych zakłada się, że~algorytm ma swobodny dostęp do~całego zbioru danych i~może je przetwarzać dowolną ilość razy (zwykle więcej niż jeden). Co więcej, zdobyta wiedza może zostać wykorzystana dopiero po~zakończeniu procesu uczenia i~nie może być później aktualizowana. Jeżeli zajdzie potrzeba uzupełnienia wiedzy o~nowe dane, trzeba uruchomić algorytm ponownie dla całego, poszerzonego zbioru uczącego~\cite{ml-deckert2013}.

Niestety takie podejście zawodzi we współczesnych systemach. Muszą one szybko przetwarzać ogromne ilości danych, które~nie zmieszczą się w~pamięci komputera. Kłóci się to z~wymaganiem swobodnego dostępu do~danych. W~zglobalizowanym świecie systemy muszą działać bez przestojów. Nie występuje w~nich ,,okres niskiej aktywności użytkowników''. Powstaje więc problem,kiedy i~jak często taki system powinien być przeuczany. Te problemy doprowadziły do~rozwoju badań nad~metodami strumieniowymi.

%------------------------------------------
\section{Metody strumieniowe}
\label{sec:metody-strumieniowe}
%------------------------------------------
\hyphenation{FLORA}
\hyphenation{VFDR}
\hyphenation{VFDT}
\hyphenation{ADWIN}
%------------------------------------------

Przetwarzanie strumieni danych jest aktualnie popularnym tematem badań. Strumienie danych charakteryzują się ogromną ilością danych (potencjalnie nieskończoną) i~dużą szybkością napływania nowych danych. Całość nie zmieści się naraz w~pamięci komputera, więc nie ma możliwości swobodnego dostępu do~nich. To, co można natomiast zrobić to np. zapamiętać część ostatnich przykładów, wybrane przykłady z~przeszłości lub~jedynie pewne statystyki o~tych danych. Takie podejście często jest stosowane przez metody strumieniowe.

Jednym z~większych wyzwań w~przetwarzaniu strumieni danych jest możliwość występowania tzw. \textbf{zmiany konceptu} (\textit{ang. concept drift}), czyli zmiany zależności w~danych, która~podważa dotychczas zdobytą wiedzę. Zmiany konceptu dzieli się na~\textbf{nagłe} (\textit{ang. sudden, abrupt}) i~\textbf{stopniowe} (\textit{ang. gradual, incremental}). Przykładowo takimi zmianami będą odpowiednio: nowy rodzaj ataku na~systemy zabezpieczające oraz~zmiana preferencji użytkownika w~czasie. Inne rodzaje warte wspomnienia to \textbf{szum} (\textit{ang. noise}),  \textbf{krótkotrwałe zakłócenia} (\textit{ang. blip}) i~\textbf{nawracające} zmiany (\textit{ang. recurrent}). Szum to drobne odchylenia od~normy, które~nie powinny mieć większego wpływu na~zdobytą wiedzę. Krótkotrwałe zakłócenia to rzadkie, znaczące odchylenia, które~powinny być traktowane jako obserwacja odstająca i~zostać zignorowane. Nawracające zmiany to odchylenia, które~pojawiają się co jakiś czas (niekoniecznie regularnie)~\cite{ml-deckert2013}. Rodzaje zmian zostały zobrazowane na~rysunku~\ref{fig:machine-learning-concept-drift}. Dobry algorytm powinien być odporny zmiany każdego typu. Nieodpowiednie reagowanie na~\textit{concept drift} z~czasem skutkuje znaczącym spadkiem jakości zdobytej wiedzy.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.75\textwidth]{img/ml-concept-drift.png}
	
    \caption{\label{fig:machine-learning-concept-drift} Rodzaje zmian konceptu~\cite{ml-brzezinski}}
\end{figure}

Ogromne ilości danych, szybkość ich napływania oraz~zmienne środowisko tworzą nowe wymagania dla algorytmów, których nie spełniają standardowe metody batchowe. Wymusiło to rozwój \textbf{algorytmów przyrostowych} (\textit{ang. incremental, online})~\cite{ml-deckert2014}. Algorytmy \textit{online} różnią się od~metod batchowych tym, że:
\begin{itemize}
\item przejście po danych następuje tylko raz -- w związku z tym, że nie ma dostępu do całości, bo nie zmieści się w pamięci komputera;
\item nie ma możliwości powrotu do poprzednich danych -- chyba że część z nich zostanie celowo zapamiętana;
\item przetwarzanie kolejnych przykładów uczących musi być szybkie -- żeby nadążać za przychodzącymi nowymi danymi;
\item użytkownik ma możliwość wykorzystania aktualnej wiedzy w każdej chwili -- algorytm powinien podać aktualnie najlepszą odpowiedź i kontynuować naukę;
\item dostosowują się do zmian konceptu.
\end{itemize}

Taki sposób uczenia jest podobny do~sposobu zdobywania wiedzy przez ludzi. Człowiek uczy się cały czas i~aktualizuje swoją wiedzę o~nowo zebrane informacje. Oczywiście, z~powodu bardziej restrykcyjnych wymagań, można się spodziewać, że~algorytmy strumieniowe będą działały trochę gorzej w~porównaniu do~algorytmów batchowych.

Nauka ze~strumienia danych w~obecności zmiennego środowiska jest stosunkowo nowym zagadnieniem w~świecie uczenia maszynowego, lecz istnieją już metody, które~starają się rozwiązać ten problem~\cite{ml-deckert2013}. Można wyróżnić trzy rodzaje takich metod. Pierwsze z~nich to algorytmy, które~już z~natury były przyrostowe i~nie wymagały większych modyfikacji. Przykładowo jest to naiwny klasyfikator bayesowski (\textit{ang. naive bayes}) i~\textit{VFDT} (\textit{Very Fast Decision Trees}, \textit{Hoeffding trees}). Kolejne to takie, które~pierwotnie nie były algorytmami \textit{online}, ale~dzięki zastosowaniu pewnych mechanizmów skutecznie je imitują. Przykładowym takim mechanizmem jest okno o~zmiennym rozmiarze (\textit{ADWIN~-- \mbox{ADaptive} \mbox{WINdow}}). Algorytm ten zapamiętuje $n$ ostatnich przykładów uczących i~tylko z~nich czerpie swoją wiedzę. W~momencie wykrycia \textit{concept driftu} drastycznie zmniejszany jest rozmiar okna, żeby szybko przystosować się do~nowych warunków. Gdy \textit{concept drift} nie występuje, rozmiar okna stopniowo zwiększa się, żeby czerpać wiedzę z~większej ilości danych~\cite{ml-brzezinski}. Ostatnim rodzajem metod strumieniowych są zaprojektowane od~podstaw algorytmy przeznaczone do~pracy na~strumieniu danych. Przykładowo są to: FLORA, \mbox{AQ11-PM+WAH}, FACIL, VFDR i~RILL~\cite{ml-deckert2013,ml-deckert2014}.

W~niniejszej pracy wykorzystane zostały algorytmy strumieniowe rozwiązujące problem klasyfikacji.